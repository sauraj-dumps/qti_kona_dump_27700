## qssi-user 13 TP1A.220905.001 1678976093046 release-keys
- Manufacturer: qualcomm
- Platform: kona
- Codename: kona
- Brand: qti
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 4.19.157
- Id: TP1A.220905.001
- Incremental: 1678976093046
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: qti/kona/kona:12/RKQ1.211119.001/1679026022928:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TP1A.220905.001-1678976093046-release-keys
- Repo: qti_kona_dump_27700
