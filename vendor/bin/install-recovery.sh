#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:4c41aee111412dfc3edc505fd2aa2a98d491ef99; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:5d29d5f5721e113c7151a103ff0acc735a10ed90 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:4c41aee111412dfc3edc505fd2aa2a98d491ef99 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
